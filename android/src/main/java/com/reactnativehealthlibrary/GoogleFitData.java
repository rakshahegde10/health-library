package com.reactnativehealthlibrary;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.facebook.react.ReactActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.HealthDataTypes;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleFitData extends ReactActivity {
  private static String TAG = "MyHealth";
  private static Date startDate;
  private static Date endDate;
  JSONObject json = new JSONObject();


  @Override
  protected String getMainComponentName() {
    return "MyHealth";
  }

  private FitnessOptions fitnessOptions;
  int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 1001;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  public void checkFitPermissions() {
    setFitnessOption();
    if (isGoogleFitPermissionGranted()) {
      Log.d("Fit", "Request Granted" );
    } else {
      requestGoogleFitPermission();
      Log.d("Fit", "Request permission" );
    }
  }

  public boolean isGoogleFitPermissionGranted() {
    if (GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
      return true;
    } else {
      return false;
    }
  }

  public void setFitnessOption() {
    fitnessOptions = FitnessOptions.builder()
      .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
      .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
      .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
      .addDataType(DataType.AGGREGATE_HEART_RATE_SUMMARY, FitnessOptions.ACCESS_READ)
      .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_WRITE)
      .addDataType(DataType.AGGREGATE_HEART_RATE_SUMMARY, FitnessOptions.ACCESS_WRITE)
      .addDataType(HealthDataTypes.TYPE_BLOOD_PRESSURE, FitnessOptions.ACCESS_READ)
      .addDataType(HealthDataTypes.AGGREGATE_BLOOD_PRESSURE_SUMMARY, FitnessOptions.ACCESS_READ)
      .addDataType(HealthDataTypes.TYPE_BLOOD_GLUCOSE, FitnessOptions.ACCESS_READ).build();

    // insertHearRate();
  }

  // public void signIn() {
  // GoogleSignInOptions googleSignInOptions = new
  // GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
  // .addExtension(fitnessOptions).requestEmail().build();
  //
  // GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this,
  // googleSignInOptions);
  // Intent signIntent = googleSignInClient.getSignInIntent();
  // GoogleSignIn.getSignedInAccountFromIntent(signIntent);
  // requestGoogleFitPermission();
  // }

  public void requestGoogleFitPermission() {

    GoogleSignInAccount account = GoogleSignIn.getAccountForExtension(this, fitnessOptions);
    GoogleSignIn.requestPermissions(this, GOOGLE_FIT_PERMISSIONS_REQUEST_CODE, account, fitnessOptions);

    ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.BODY_SENSORS },
      GOOGLE_FIT_PERMISSIONS_REQUEST_CODE);

    Boolean sign = isSignedIn();
    Log.v("signed in???", String.valueOf(sign));

  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == RESULT_OK) {
      Log.e(TAG, "onActivityResult Permission granted");
    } else if (resultCode == RESULT_CANCELED) {
      Log.e(TAG, " onActivityResult Result Cancelled code: " + resultCode);
    } else {
      Log.e(TAG, " onActivityResult Result code unknown: " + resultCode);
    }
  }

  private void insertHearRate() {

    Calendar cal = Calendar.getInstance();

    Date now = new Date();
    cal.setTime(now);
    long endTime = cal.getTimeInMillis();
    cal.add(Calendar.SECOND, -1);
    long startTime = cal.getTimeInMillis();

    // Create a data source
    DataSource dataSource = new DataSource.Builder().setAppPackageName("com.nativehealth")
      .setDataType(DataType.TYPE_HEART_RATE_BPM).setType(DataSource.TYPE_RAW).build();

    // Create a data set
    float heartRate = 81;
    DataPoint dataPoint = DataPoint.builder(dataSource).setField(Field.FIELD_BPM, heartRate)
      .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS).build();

    DataSet dataSet = DataSet.builder(dataSource).add(dataPoint).build();

    Fitness.getHistoryClient(this, GoogleSignIn.getAccountForExtension(this, fitnessOptions)).insertData(dataSet)
      .addOnSuccessListener(new OnSuccessListener() {
        @Override
        public void onSuccess(Object o) {
          Log.v(TAG, "Successfully added the dataset!!");
        }
      }).addOnFailureListener(new OnFailureListener() {
      @Override
      public void onFailure(@NonNull Exception e) {
        Log.v(TAG, "Failed to add dataset!!!");
      }
    });
  }

  private boolean isSignedIn() {
    return GoogleSignIn.getLastSignedInAccount(this) != null;
  }

  public Task<DataReadResponse> readHistoryData() {

    DataReadRequest readRequest = queryHRFitnessData();
    return Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this)).readData(readRequest)
      .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
        @Override
        public void onSuccess(DataReadResponse dataReadResponse) {
          Log.v(TAG, "Success, try printData " + dataReadResponse);
          try {
            printData(dataReadResponse);
          } catch (JSONException e) {
            Log.v(TAG, "Fail");
          }
        }
      }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
          Log.v(TAG, "Failture");
        }
      });
  }

  public static DataReadRequest queryHRFitnessData() {
    Calendar cal = Calendar.getInstance();
    Calendar cal1 = Calendar.getInstance();
    Date now = new Date();
    cal1.setTime(endDate);
    cal1.set(Calendar.HOUR_OF_DAY, 23);
    cal1.set(Calendar.MINUTE, 59);
    cal1.set(Calendar.SECOND, 59);
    cal1.set(Calendar.MILLISECOND, 999);

    long endTime = cal1.getTimeInMillis();

    cal.setTime(startDate);
    long startTime = cal.getTimeInMillis();

    Log.d("dateStart!!!!!!!!!!", String.valueOf(startTime));
    Log.d("dateEnd!!!!!!!!!!!", String.valueOf(endTime));

    java.text.DateFormat dateFormat = DateFormat.getDateInstance();

    DataReadRequest readRequest = new DataReadRequest.Builder()
      //.aggregate(DataType.TYPE_HEART_RATE_BPM)
      .aggregate(HealthDataTypes.TYPE_BLOOD_PRESSURE)
      .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS).bucketByTime(1, TimeUnit.DAYS).build();
    Log.v(TAG, "readRequest: " + readRequest);
    return readRequest;
  }

  public void printData(DataReadResponse dataReadResult) throws JSONException {
    Log.v(TAG, "dataReadResult.getBuckets().size()" + dataReadResult.getBuckets().size());
    Log.v(TAG, "dataReadResult.getDataSets().toString()" + dataReadResult.getDataSets().toString());
    Log.v(TAG, "dataReadResult.getStatus()" + dataReadResult.getStatus());
    Log.v(TAG, "dataReadResult.getDataSet(DataType.TYPE_HEART_RATE_BPM)" + dataReadResult.getDataSet(DataType.TYPE_HEART_RATE_BPM));

    if (dataReadResult.getBuckets().size() > 0) {
      for (Bucket bucket : dataReadResult.getBuckets()) {
        List<DataSet> dataSets = bucket.getDataSets();
        Log.v(TAG, "Datasets: " + dataSets);
        for (DataSet dataSet : dataSets) {
          dumpDataSet(dataSet);
        }
      }
    } else if (dataReadResult.getDataSets().size() <= 0) {
      System.out.print("Number of returned DataSets is: " + dataReadResult.getDataSets().size());
      for (DataSet dataSet : dataReadResult.getDataSets()) {
        dumpDataSet(dataSet);
      }
    }
  }

  private void dumpDataSet(DataSet dataSet) throws JSONException {
    json = new JSONObject();
    String sname, dname, hname;
    JSONObject jsonObject = new JSONObject();
    Log.v(TAG, "Name: " + dataSet.getDataType().getName());
    // DateFormat dateFormat = DateFormat.getTimeInstance();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS z");
    Log.v(TAG, "Fields: " + dataSet.getDataSource().getDataType().getFields());
    Log.v(TAG, "dumpDataSet Data Point Values :" + dataSet.getDataPoints());
    Log.v(TAG, "dumpDataSet Type: " + dataSet.getDataType().getName());
    for (DataPoint dp : dataSet.getDataPoints()) {
      Log.v(TAG, "dumpDataSet Data Point: " + dp);
      Log.v(TAG, "dumpDataSet Data Point hashCode: " + dp.hashCode());
      Log.v(TAG, "dumpDataSet Data Point getDataSource: " + dp.getDataSource());
      Log.v(TAG, "dumpDataSet Data Point getOriginalDataSource: " + dp.getOriginalDataSource());
      Log.v(TAG, "dumpDataSet Data Point: getTimestamp: " + dateFormat.format(dp.getTimestamp(TimeUnit.MILLISECONDS)));
      Log.v(TAG, "BP dumpDataSet Start: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
      Log.v(TAG, "BP dumpDataSet End: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));

      jsonObject.put("dataType", dataSet.getDataType().getName().toString());
      jsonObject.put("startDate", dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)).toString());
      jsonObject.put("endDate", dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)).toString());

      for (Field field : dp.getDataType().getFields()) {
        Log.v(TAG, "Field: " + field.getName() + ", Value : " + dp.getValue(field).toString());
        if (dataSet.getDataType().getName().contains("com.google.blood_pressure.summary")) {
          sname = field.getName();
          if (sname.equals("blood_pressure_systolic_average")) {
            jsonObject.put("Systolic", dp.getValue(field).toString());
          }
          dname = field.getName();
          if (dname.equals("blood_pressure_diastolic_average")) {
            jsonObject.put("Diastolic", dp.getValue(field).toString());
          }
          json.put("BloodPressure", jsonObject);
        } else {
          hname = field.getName();
          if (hname.equals("average")) {
            jsonObject.put("HeartRate", dp.getValue(field).toString());
          }
          json.put("HeartRate", jsonObject);
        }
      }

    }
  }

  public JSONObject getHealthValues() {
    readHistoryData();
    Log.v(TAG, "JSON: " + json);
    return json;
  }

  public void getTime(String time) {
    Log.v(TAG, "time inside: " + time);
    String dtStart = time.substring(0, time.indexOf(","));
    String dtEnd = time.substring(time.indexOf(",")+1);
    SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd yyyy");
    try {
      Date dateStart = format.parse(dtStart);
      Log.d("dateStart", String.valueOf(dateStart));
      startDate = dateStart;
      Date dateEnd = format.parse(dtEnd);
      Log.d("dateEnd", String.valueOf(dateEnd));
      endDate = dateEnd;
    } catch (ParseException e) {
      Log.d("date", "format not supported "+ dtStart);
      //e.printStackTrace();
    }
  }


}
