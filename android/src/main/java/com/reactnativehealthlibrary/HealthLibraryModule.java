package com.reactnativehealthlibrary;

import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import org.json.JSONObject;


public class HealthLibraryModule extends ReactContextBaseJavaModule {

  private GoogleFitData googleFitData = null;

  public HealthLibraryModule(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  //Mandatory function getName that specifies the module name
      @Override
      public String getName() {
        return "HealthLibrary";
      }

      @ReactMethod
      public void getDeviceName(Callback cb) {
        try{
          cb.invoke(null, android.os.Build.MODEL);
        }catch (Exception e){
          cb.invoke(e.toString(), null);
        }
      }

      @ReactMethod
      public void multiply(int a, int b, Promise promise){
       promise.resolve(a*b);
      }

      @ReactMethod
      public void getAuthorization() {
        try{
          googleFitData.checkFitPermissions();
        }catch (Exception e){
          Log.i("error", "error!!!!");
        }
      }

      @ReactMethod
      public void getHealthData(Callback cb) {
        try{
            JSONObject obj = googleFitData.getHealthValues();
            cb.invoke(null, String.valueOf(obj));
        }catch (Exception e){
          Log.i("error", "error here!!!!");
          cb.invoke(e.toString(), null);
        }
      }





}
