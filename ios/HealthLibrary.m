#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(MyHealthLibrary, NSObject)

RCT_EXTERN_METHOD(requestAuthorizations)

RCT_EXTERN_METHOD(requestAuthorization:(NSDictionary)toShare
                  read:NSDictionary
                  resolve:(RCTPromiseResolveBlock)resolve
                  withRejecter:(RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(setTime: (NSString*)time)

RCT_EXTERN_METHOD(getHealthData: (NSString*)time
                                type: (NSString*)type
                                data: (NSDictionary*)data
                                resolver: (RCTPromiseResolveBlock)resolve
                                rejecter: (RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(requestAuthorizations)


//RCT_EXTERN_METHOD(getBloodType: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)
//
//RCT_EXTERN_METHOD(getAge: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)
//
//RCT_EXTERN_METHOD(getSteps: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)
//
//RCT_EXTERN_METHOD(getBloodPressure: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)
//
//RCT_EXTERN_METHOD(getHeartRate: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)
//
//RCT_EXTERN_METHOD(getNativeData: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)
//
//RCT_EXTERN_METHOD(getBMI: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)
//
//RCT_EXTERN_METHOD(getBloodGlucose: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)
//
//RCT_EXTERN_METHOD(getBodyTemperature: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)
//
//RCT_EXTERN_METHOD(getAverage: (RCTPromiseResolveBlock)resolve rejecter: (RCTPromiseRejectBlock)reject)

@end
