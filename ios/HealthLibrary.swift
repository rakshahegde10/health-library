import Foundation
import HealthKit


let GENERIC_ERROR = "HEALTHKIT_ERROR";
let HKQuantityTypeIdentifier_PREFIX = "HKQuantityTypeIdentifier"


extension JSON{
    mutating func appendIfArray(json:JSON){
        if var arr = self.array{
            arr.append(json)
            self = JSON(arr);
        }
    }
    
    mutating func appendIfDictionary(key:String,json:JSON){
        if var dict = self.dictionary{
            dict[key] = json;
            self = JSON(dict);
        }
    }
}

extension Calendar {

    func endOfDay(for date: Date) -> Date {

        let dayStart = self.startOfDay(for: date)
        guard let nextDayStart = self.date(byAdding: .day, value: 1, to: dayStart) else {
            preconditionFailure("Expected start of next day")
        }
        var components = DateComponents()
        components.second = -1
        guard let dayEnd = self.date(byAdding: components, to: nextDayStart) else {
            preconditionFailure("Expected end of day")
        }
        return dayEnd
    }

}

@objc(MyHealthLibrary)
class MyHealthLibrary: NSObject {
    
    let healthStore = HKHealthStore()
        
        func objectTypeFromString(typeIdentifier: String) -> HKObjectType? {
            print("typeid:", typeIdentifier)

            if(typeIdentifier.starts(with: HKQuantityTypeIdentifier_PREFIX)){
                let identifier = HKQuantityTypeIdentifier.init(rawValue: typeIdentifier);
                return HKObjectType.quantityType(forIdentifier: identifier) as HKObjectType?
            }

            return nil;
        }
        
        func sampleTypeFromString(typeIdentifier: String) -> HKSampleType? {
            if(typeIdentifier.starts(with: HKQuantityTypeIdentifier_PREFIX)){
                let identifier = HKQuantityTypeIdentifier.init(rawValue: typeIdentifier);
                return HKSampleType.quantityType(forIdentifier: identifier) as HKSampleType?
            }
            
            return nil;
        }
        
        func objectTypesFromDictionary(typeIdentifiers: NSDictionary) -> Set<HKObjectType> {
            var share = Set<HKObjectType>();
            for item in typeIdentifiers {
                if(item.value as! Bool){
                    let objectType = objectTypeFromString(typeIdentifier: item.key as! String);
                    if(objectType != nil){
                        share.insert(objectType!);
                    }
                }
            }
            return share;
        }
        
        func sampleTypesFromDictionary(typeIdentifiers: NSDictionary) -> Set<HKSampleType> {
            var share = Set<HKSampleType>();
            for item in typeIdentifiers {
                if(item.value as! Bool){
                    let sampleType = sampleTypeFromString(typeIdentifier: item.key as! String);
                    if(sampleType != nil){
                     share.insert(sampleType!);
                    }
                }
            }
            return share;
        }
    
    enum HealthDataType: String, Codable {
          case heartRate
          case bmi
          case bloodpressure
          case biologicalSex
          case stepCount
          case bloodGlucose
          case bodyTemperature
          case none
      }


      struct HealthDataItem: Codable {
          let endDate: String
          let value: String
          let startDate: String
          let uuid: String
          let type: String
      }


      var myJSON: JSON = [
          "myDictionary": [String:AnyObject]()
      ]

      var myArray: [String] = [ ]

      var startDate: Date = Date()
      var endDate: Date = Date()

      var healthType: String = ""


    // Function Method to request Authorizations from Healthkit
        @objc(requestAuthorization:read:resolve:withRejecter:)
        func requestAuthorization(toShare: NSDictionary, read: NSDictionary, resolve: @escaping RCTPromiseResolveBlock,reject: @escaping RCTPromiseRejectBlock) {
            
            let share = sampleTypesFromDictionary(typeIdentifiers: toShare);
            let toRead = objectTypesFromDictionary(typeIdentifiers: read);
            
            print("read:", toRead)
             
                healthStore.requestAuthorization(toShare: share, read: toRead) { (success: Bool, error: Error?) in
                    guard let err = error else {
                        return resolve(success);
                    }
                    reject(GENERIC_ERROR, err.localizedDescription, err);
                }
      }
    
    
                   func getPermsFromOptions(_ options: NSArray) -> Set<HKObjectType> {
                    var readPermSet: Set<HKObjectType> = []
                    var optionKey: String
                    var val: HKObjectType
                  
                  for i in options{
                    optionKey = i as! String
                    val = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier(rawValue: optionKey))!
                    readPermSet.insert(val)
                    print("set", readPermSet)
                          }
                        return readPermSet;
                      }

    
    
    
                  @objc
                  func setTime(_ time: String) -> Void {
                                  print("Time:", time);
                                  let pre = String(time.prefix(16))
                                  let suf = String(time.suffix(16))
                              //    let dateFormatter = DateFormatter()
                              //    dateFormatter.dateFormat = "EEE MMM dd yyyy hh:mm:ss"
                              //    let fromDate = dateFormatter.date(from: pre)
                              //    startDate = fromDate!
                              //    let toDate = dateFormatter.date(from: suf)
                              //    endDate = toDate!
                                  print(suf, pre)
                                  let fromDate = convertStringintoDate(dateStr: pre, dateFormat: "EEE MMM dd yyyy")
                                  startDate = fromDate
                                  let toDate = convertStringintoDate(dateStr: suf, dateFormat: "EEE MMM dd yyyy")
                                  endDate = toDate
                                  print(startDate, endDate)
                                  _ = HealthDataItem(endDate: "", value: "", startDate: "", uuid: "", type: "")
                                  self.myJSON["myDictionary"].appendIfDictionary(key:"HD", json: JSON([]))
                                }


                  
                  func convertStringintoDate(dateStr:String, dateFormat format:String) -> Date{
                      let dateFormatter = DateFormatter()
                      dateFormatter.locale = Locale.init(identifier:"en_US_POSIX")
                      dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT")
                      dateFormatter.dateFormat = format
                      if let startDate = dateFormatter.date(from: dateStr){
                          return startDate as Date
                      }
                      return Date() as Date
                  }

    
    
              @objc(getHealthData:type:data:resolver:rejecter:)
              func getHealthData( _ time: String, type: String, data: NSDictionary, resolver resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock){
              setTime(time)
              print("object:", data)
              let object = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier(rawValue: type))
              let startOfDate = Calendar.current.startOfDay(for: startDate)
              let endOfDate = Calendar.current.endOfDay(for: endDate)
              print("date:",startOfDate, endOfDate)
              let predicate = HKQuery.predicateForSamples(withStart: startOfDate, end: endOfDate, options: .strictStartDate)
              let query = HKSampleQuery(sampleType: object!, predicate: predicate, limit: 20, sortDescriptors: nil) { (query, results, error) in
                          for result in results as! [HKQuantitySample] {
                            let quan = result.quantity.description
                            let uuid = result.uuid.description
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMM dd yyyy hh:mm:ss"
                            let strStart = dateFormatter.string(from: result.startDate)
                            let strEnd = dateFormatter.string(from: result.endDate)
                              let heartRateItem = HealthDataItem(endDate: strEnd, value: String(quan), startDate: strStart, uuid:String(uuid), type: type)
                            do {
                                      let data = try JSONEncoder().encode(heartRateItem)
                                      let json = String(data: data, encoding: String.Encoding.utf8)
                                          if(self.myJSON["myDictionary"]["HD"].exists()){
                                            self.myJSON["myDictionary"]["HD"].appendIfArray(json: JSON(json as Any))
                                          } else {
                                            self.myJSON["myDictionary"].appendIfDictionary(key:"HD", json: JSON([json]))
                                          }
                                } catch {
                                  //error handling
                                }
                          }
                              print("json xcode:", self.myJSON)
                             
                do {
                  let data = try JSONEncoder().encode(self.myJSON["myDictionary"]["HD"])
                  let convertedString = String(data: data, encoding: String.Encoding.utf8)
                  resolve(convertedString)
                } catch {
                  
                }
                      }
                healthStore.execute(query)

                }
    
    
    // Function Method to request Authorizations from Healthkit
            @objc
            func requestAuthorizations() {
               let writeDataTypes : Set<HKSampleType> = [ HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!,
                                                          HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureDiastolic)!,
                                                          HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic)!,
                                                          HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!,
                                                          HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodGlucose)!,
                                                          HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyTemperature)!
                                                          ]
               
               let readDataTypes : Set = [HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!,
                                                    HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!,
                                                    HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureDiastolic)!,
                                                    HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic)!,
                                                    HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.biologicalSex)!,
                                                    HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!,
                                                    HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodGlucose)!,
                                                    HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyTemperature)!,
                                                    HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.bloodType)!,
                                                    HKObjectType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.dateOfBirth)!]

                         
                healthStore.requestAuthorization(toShare: writeDataTypes, read: readDataTypes) { (success, error) in
                           //
                           
                             if !success {
                                 // Handle the error here.
                             }
                         }
               }

              
//              @objc(getBloodType:rejecter:)
//              func getBloodType(_ resolve: RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
//                var bloodType: String? {
//                  if try! healthStore.bloodType().bloodType == HKBloodType.aNegative {
//                           return "A-"
//                  } else if try! healthStore.bloodType().bloodType == HKBloodType.aPositive {
//                          return "A+"
//                  } else if try! healthStore.bloodType().bloodType == HKBloodType.abNegative {
//                          return "AB-"
//                        }else if try! healthStore.bloodType().bloodType == HKBloodType.abPositive {
//                          return "AB+"
//                        }else if try! healthStore.bloodType().bloodType == HKBloodType.bNegative {
//                          return "B-"
//                        }else if try! healthStore.bloodType().bloodType == HKBloodType.bPositive {
//                          return "B+"
//                        }else if try! healthStore.bloodType().bloodType == HKBloodType.oPositive {
//                          return "O+"
//                        }else if try! healthStore.bloodType().bloodType == HKBloodType.oNegative {
//                          return "O-"
//                        }
//                      return nil
//                    }
//                    print("blood grp:", bloodType as Any)
//                    resolve(bloodType)
//                }
//
//              @objc(getAge:rejecter:)
//              func getAge(_ resolve: RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
//                if #available(iOS 10.0, *) {
//                  guard let birthdayComponents =  try? healthStore.dateOfBirthComponents() else {
//                    resolve("")
//                    return
//                  }
//                  let today = Date()
//                      let calendar = Calendar.current
//                      let todayDateComponents = calendar.dateComponents([.year],
//                                                                          from: today)
//                      let thisYear = todayDateComponents.year!
//                      let age = thisYear - birthdayComponents.year!
//
//                      resolve(age)
//                } else {
//                  // Fallback on earlier versions
//                }
//
//                }
//
//            //  @objc
//            //  func getSteps(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
//            //
//            //    let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
//            //        let now = Date()
//            //        let startOfDay = Calendar.current.startOfDay(for: now)
//            //        var interval = DateComponents()
//            //        interval.day = 1
//            //
//            //        let query = HKStatisticsCollectionQuery(quantityType: type,
//            //                                               quantitySamplePredicate: nil,
//            //                                               options: [.cumulativeSum],
//            //                                               anchorDate: startOfDay,
//            //                                               intervalComponents: interval)
//            //
//            //            query.initialResultsHandler = { _, result, error in
//            //                    var resultCount = 0.0
//            //                    result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
//            //
//            //                    if let sum = statistics.sumQuantity() {
//            //                        // Get steps (they are of double type)
//            //                        resultCount = sum.doubleValue(for: HKUnit.count())
//            //                    } // end if
//            //                      print("result in xcode:", resultCount)
//            //                      let stepCountItem = HealthDataItem(endDate: now, value: String(resultCount), startDate: startOfDay, type: .stepCount)
//            //                              let healthData = [stepCountItem]
//            //                              do {
//            //                                let data = try JSONEncoder().encode(healthData)
//            //                                let json = String(data: data, encoding: String.Encoding.utf8)
//            //
//            //                                print("json data:", json as Any )
//            //                                resolve(json)
//            //                              } catch {
//            //                                   //error handling
//            //                              }
//            //                }
//            //            }
//            //        healthStore.execute(query)
//            //      }
//            //
//
//
//
//              @objc(getBloodPressure:rejecter:)
//              func getBloodPressure(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
//                var value : String = ""
//                guard let type = HKQuantityType.correlationType(forIdentifier: HKCorrelationTypeIdentifier.bloodPressure),
//                            let systolicType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic),
//                            let diastolicType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureDiastolic) else {
//
//                                return
//                        }
//                let startOfDate = Calendar.current.startOfDay(for: startDate)
//                let endOfDate = Calendar.current.endOfDay(for: endDate)
//                     let predicate = HKQuery.predicateForSamples(withStart: startOfDate, end: endOfDate, options: .strictStartDate)
//
//                        let sampleQuery = HKSampleQuery(sampleType: type, predicate: predicate, limit: 20, sortDescriptors: nil) { (sampleQuery, results, error) in
//                            if let dataList = results as? [HKCorrelation] {
//                                for data in dataList
//                                {
//                                    if let data1 = data.objects(for: systolicType).first as? HKQuantitySample,
//                                        let data2 = data.objects(for: diastolicType).first as? HKQuantitySample {
//
//                                        let value1 = data1.quantity.doubleValue(for: HKUnit.millimeterOfMercury())
//                                        let value2 = data2.quantity.doubleValue(for: HKUnit.millimeterOfMercury())
//                                        let uuid = data1.uuid.description
//                                        print("uuid BP:", uuid)
//                                      let dateFormatter = DateFormatter()
//                                      dateFormatter.dateFormat = "MMM dd yyyy hh:mm:ss"
//                                      let strStart = dateFormatter.string(from: data1.startDate)
//                                      let strEnd = dateFormatter.string(from: data1.endDate)
//                                        value = "\(value1) | \(value2)"
//                                        print("BP in xcode:", value)
//                                        let bloodPressureItem = HealthDataItem(endDate: strEnd, value: value, startDate: strStart, uuid:String(uuid), type: .bloodpressure)
//                                              let healthData = [bloodPressureItem]
//                                              do {
//                                                let data = try JSONEncoder().encode(healthData)
//                                                let json = String(data: data, encoding: String.Encoding.utf8)
//                                                print("json data:", json as Any )
//                                                if(self.myJSON["myDictionary"]["BP"].exists()){
//                                                  self.myJSON["myDictionary"]["BP"].appendIfArray(json: JSON(json as Any))
//                                                } else {
//                                                  self.myJSON["myDictionary"].appendIfDictionary(key:"BP", json: JSON([json]))
//                                                }
//                                              } catch {
//                                                   //error handling
//                                              }
//                                    }
//                                }
//                            }
//                          print("json xcode:", self.myJSON)
//
//                                do {
//                                  let data = try JSONEncoder().encode(self.myJSON["myDictionary"]["BP"])
//                                  let convertedString = String(data: data, encoding: String.Encoding.utf8)
//                                  resolve(convertedString)
//                                } catch {
//
//                                }
//                        }
//                        healthStore.execute(sampleQuery)
//
//
//                  }
//
//
//                @objc(getHeartRate:rejecter:)
//                func getHeartRate(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock){
//                let heartRate = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
//                let startOfDate = Calendar.current.startOfDay(for: startDate)
//                let endOfDate = Calendar.current.endOfDay(for: endDate)
//                print("dat:",startOfDate, endOfDate)
//                let predicate = HKQuery.predicateForSamples(withStart: startOfDate, end: endOfDate, options: .strictStartDate)
//                let query = HKSampleQuery(sampleType: heartRate!, predicate: predicate, limit: 20, sortDescriptors: nil) { (query, results, error) in
//                            for result in results as! [HKQuantitySample] {
//                              let heartRate = result.quantity.description
//                              let uuid = result.uuid.description
//                              print("uuid HR:", uuid)
//                              let dateFormatter = DateFormatter()
//                              dateFormatter.dateFormat = "MMM dd yyyy hh:mm:ss"
//                              let strStart = dateFormatter.string(from: result.startDate)
//                              let strEnd = dateFormatter.string(from: result.endDate)
//                                let heartRateItem = HealthDataItem(endDate: strEnd, value: String(heartRate), startDate: strStart, uuid:String(uuid), type: .heartRate)
//                              do {
//                                        let data = try JSONEncoder().encode(heartRateItem)
//                                        let json = String(data: data, encoding: String.Encoding.utf8)
//                                            if(self.myJSON["myDictionary"]["HR"].exists()){
//                                              self.myJSON["myDictionary"]["HR"].appendIfArray(json: JSON(json as Any))
//                                            } else {
//                                              self.myJSON["myDictionary"].appendIfDictionary(key:"HR", json: JSON([json]))
//                                            }
//                                  } catch {
//                                    //error handling
//                                  }
//                            }
//                                print("json xcode:", self.myJSON)
//
//                  do {
//                    let data = try JSONEncoder().encode(self.myJSON["myDictionary"]["HR"])
//                    let convertedString = String(data: data, encoding: String.Encoding.utf8)
//                    resolve(convertedString)
//                  } catch {
//
//                  }
//                        }
//                        healthStore.execute(query)
//
//                  }
//
//              @objc(getBMI:rejecter:)
//              func getBMI(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
//                let bodyMassIndex = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)
//                let startOfDate = Calendar.current.startOfDay(for: startDate)
//                let endOfDate = Calendar.current.endOfDay(for: endDate)
//                print("dat:",startOfDate, endOfDate)
//                let predicate = HKQuery.predicateForSamples(withStart: startOfDate, end: endOfDate, options: .strictStartDate)
//                let query = HKSampleQuery(sampleType: bodyMassIndex!, predicate: predicate, limit: 20, sortDescriptors: nil) { (query, results, error) in
//                            for result in results as! [HKQuantitySample] {
//                              let bodyMassIndex = result.quantity.description
//                              let uuid = result.uuid.description
//                              let dateFormatter = DateFormatter()
//                              dateFormatter.dateFormat = "MMM dd yyyy hh:mm:ss"
//                              let strStart = dateFormatter.string(from: result.startDate)
//                              let strEnd = dateFormatter.string(from: result.endDate)
//                                let bodyMassIndexItem = HealthDataItem(endDate: strEnd, value: String(bodyMassIndex), startDate: strStart, uuid:String(uuid), type: .bmi)
//                              do {
//                                        let data = try JSONEncoder().encode(bodyMassIndexItem)
//                                        let json = String(data: data, encoding: String.Encoding.utf8)
//                                            if(self.myJSON["myDictionary"]["BMI"].exists()){
//                                              self.myJSON["myDictionary"]["BMI"].appendIfArray(json: JSON(json as Any))
//                                            } else {
//                                              self.myJSON["myDictionary"].appendIfDictionary(key:"BMI", json: JSON([json]))
//                                            }
//                                  } catch {
//                                    //error handling
//                                  }
//                            }
//                                print("json xcode:", self.myJSON)
//
//                  do {
//                    let data = try JSONEncoder().encode(self.myJSON["myDictionary"]["BMI"])
//                    let convertedString = String(data: data, encoding: String.Encoding.utf8)
//                    resolve(convertedString)
//                  } catch {
//
//                  }
//                        }
//                        healthStore.execute(query)
//
//                  }
//
//              @objc(getBloodGlucose:rejecter:)
//              func getBloodGlucose(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
//                let glucoseType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodGlucose)
//                let startOfDate = Calendar.current.startOfDay(for: startDate)
//                let endOfDate = Calendar.current.endOfDay(for: endDate)
//                print("dat:",startOfDate, endOfDate)
//                let predicate = HKQuery.predicateForSamples(withStart: startOfDate, end: endOfDate, options: .strictStartDate)
//                let query = HKSampleQuery(sampleType: glucoseType!, predicate: predicate, limit: 20, sortDescriptors: nil) { (query, results, error) in
//                            for result in results as! [HKQuantitySample] {
//                              let bloodGlucose = result.quantity.description
//                              let uuid = result.uuid.description
//                              let dateFormatter = DateFormatter()
//                              dateFormatter.dateFormat = "MMM dd yyyy hh:mm:ss"
//                              let strStart = dateFormatter.string(from: result.startDate)
//                              let strEnd = dateFormatter.string(from: result.endDate)
//                                let bloodGlucoseItem = HealthDataItem(endDate: strEnd, value: String(bloodGlucose), startDate: strStart, uuid:String(uuid), type: .bloodGlucose)
//                              do {
//                                        let data = try JSONEncoder().encode(bloodGlucoseItem)
//                                        let json = String(data: data, encoding: String.Encoding.utf8)
//                                            if(self.myJSON["myDictionary"]["BG"].exists()){
//                                              self.myJSON["myDictionary"]["BG"].appendIfArray(json: JSON(json as Any))
//                                            } else {
//                                              self.myJSON["myDictionary"].appendIfDictionary(key:"BG", json: JSON([json]))
//                                            }
//                                  } catch {
//                                    //error handling
//                                  }
//                            }
//                                print("json xcode BG:", self.myJSON)
//
//                  do {
//                    let data = try JSONEncoder().encode(self.myJSON["myDictionary"]["BG"])
//                    let convertedString = String(data: data, encoding: String.Encoding.utf8)
//                    resolve(convertedString)
//                  } catch {
//
//                  }
//                        }
//                        healthStore.execute(query)
//
//                  }
//
//              @objc(getBodyTemperature:rejecter:)
//              func getBodyTemperature(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
//                let bodyTemperature = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyTemperature)
//                let startOfDate = Calendar.current.startOfDay(for: startDate)
//                let endOfDate = Calendar.current.endOfDay(for: endDate)
//                print("dat:",startOfDate, endOfDate)
//                let predicate = HKQuery.predicateForSamples(withStart: startOfDate, end: endOfDate, options: .strictStartDate)
//                let query = HKSampleQuery(sampleType: bodyTemperature!, predicate: predicate, limit: 20, sortDescriptors: nil) { (query, results, error) in
//                            for result in results as! [HKQuantitySample] {
//                              let bodyTemperature = result.quantity.description
//                              let uuid = result.uuid.description
//                              let dateFormatter = DateFormatter()
//                              dateFormatter.dateFormat = "MMM dd yyyy hh:mm:ss"
//                              let strStart = dateFormatter.string(from: result.startDate)
//                              let strEnd = dateFormatter.string(from: result.endDate)
//                                let bodyTemperatureItem = HealthDataItem(endDate: strEnd, value: String(bodyTemperature), startDate: strStart, uuid:String(uuid), type: .bodyTemperature)
//                              do {
//                                        let data = try JSONEncoder().encode(bodyTemperatureItem)
//                                        let json = String(data: data, encoding: String.Encoding.utf8)
//                                            if(self.myJSON["myDictionary"]["BT"].exists()){
//                                              self.myJSON["myDictionary"]["BT"].appendIfArray(json: JSON(json as Any))
//                                            } else {
//                                              self.myJSON["myDictionary"].appendIfDictionary(key:"BT", json: JSON([json]))
//                                            }
//                                  } catch {
//                                    //error handling
//                                  }
//                            }
//                                print("json xcode:", self.myJSON)
//
//                  do {
//                    let data = try JSONEncoder().encode(self.myJSON["myDictionary"]["BT"])
//                    let convertedString = String(data: data, encoding: String.Encoding.utf8)
//                    resolve(convertedString)
//                  } catch {
//
//                  }
//                        }
//                        healthStore.execute(query)
//
//                  }
              
              func jsonToString(json: AnyObject) -> String{
                  do {
                      let data1 = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
                      let convertedString = String(data: data1, encoding: String.Encoding.utf8) as NSString? ?? ""
                      debugPrint(convertedString)
                      return convertedString as String
                  } catch let myJSONError {
                      debugPrint(myJSONError)
                      return ""
                  }
              }
              
//              @objc(getAverage:rejecter:)
//              func getAverage(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
//                let glucoseType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodGlucose)
//                var savg : String = ""
//                var interval = DateComponents()
//                interval.day = 200
//                let startOfDate = Calendar.current.startOfDay(for: startDate)
//                let endOfDate = Calendar.current.endOfDay(for: endDate)
//                let query = HKStatisticsCollectionQuery(quantityType: glucoseType!,
//                                                       quantitySamplePredicate: nil,
//                                                       options: [.discreteAverage],
//                                                       anchorDate: startOfDate,
//                                                       intervalComponents: interval)
//                        query.initialResultsHandler = { _, result, error in
//
//                                result!.enumerateStatistics(from: startOfDate, to: endOfDate) { statistics, _ in
//                                  let avg = statistics.averageQuantity()?.description
//                                  savg = avg!
//                                  print("avg in xcode:", savg)
//                                  resolve(savg)
//                                }}
//                healthStore.execute(query)
//              }

            @objc
            override static func requiresMainQueueSetup() -> Bool {
            return true
           }


}
