// import type {ReactNativeHealthkit} from './Auth';
// import Native from './AuthType';
// import type { HKQuantityTypeIdentifier } from './AuthType';

// const requestAuthorization = (
// 	read: (HKQuantityTypeIdentifier) [],
// 	write: HKQuantityTypeIdentifier[] = []
//   ): Promise<boolean> => {
// 	const readAuth = read.reduce((obj, cur) => {
// 		return { ...obj, [cur]: true };
// 	  }, {});
  
// 	const writeAuth = write.reduce((obj, cur) => {
// 		return { ...obj, [cur]: true };
// 	  }, {});
  
// 	return Native.requestAuthorization(writeAuth, readAuth);
//   };

//   const MyHealthLibrary: ReactNativeHealthkit = {
// 	requestAuthorization
//   }

//   export * from './Auth';
//   export * from './AuthType';

//   export default MyHealthLibrary;

import { NativeModules } from 'react-native';
  
const { HealthLibrary } = NativeModules;

export default HealthLibrary as any;

  



